# News agregator test project

### Docker setup

 - Install [docker](https://docs.docker.com/engine/installation/) v.1.12 or later and [docker-compose](https://docs.docker.com/compose/install/) v.1.8 or later. Restart is required by the docker daemon.
 - Go to project docker folder and build symfony and database containers:

```sh
{project}/docker$ docker-compose build symfony
{project}/docker$ docker-compose build db
```

 - Start containers and wait for database to initialize:

```sh
{project}/docker$ docker-compose up
```

 - Kill containers (Ctrl+C) and remove them ('y' when asked if sure):

```sh
{project}/docker$ docker-compose kill
{project}/docker$ docker-compose rm
```

 - Open docker-compose.yml file and uncomment db mounted volumes - db configuration should look like follows:

```sh
   db:
        build: ./db
        container_name: db
        volumes:
            - ./data/mysql:/var/lib/mysql
```

 - Start containers again:

```sh
{project}/docker$ docker-compose up
```

 If you want to start them in background use this instead:

```sh
{project}/docker$ docker-compose up -d
```

### Symfony setup
Go to source dir and install vendors:

```sh
{project}/src$ composer install
```

Back in docker dir run:

```sh
{project}/docker$ ./init.sh
```

Go to http://localhost:8000/app_dev.php/login and use username 'test' and password '123456' to login.

### Running scrapers

To run scrapers, enter symfony container and run existing console command with one of the following options: btv, novinite, vesti:

```sh
{project}/docker$ docker exec -it symfony bash
root@8bedafad3dcc:/app# php bin/console scraper:run btv
```

After the script finishes, go to latest news list: http://localhost:8000/app_dev.php/news/latest and enjoy.

### Useful docker commands

```sh
 # check running containers
{project}/docker$ docker ps
 # view container logs
{project}/docker$ docker-compose logs {container_name}
```

### Running project locally

  - Change db connection details in parameters.yml
 - Create and update database schema manually. Create user from command line. Install assets. Use local server port with project dev routing.
 - NB: Be careful to configure your mysql server instance to use utf8mb4 encoding and collation, since default utf8 in MySQL 5.7 does not support the full utf8 range but uses a maximum of three bytes per character and does not cover the standard utf8 encoding. Sample config:

```sh
[client]
default-character-set = utf8mb4
[mysql]
default-character-set = utf8mb4
[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
```
