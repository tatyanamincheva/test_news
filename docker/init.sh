#!/bin/bash
# update db
docker exec -i symfony php bin/console doctrine:schema:update --force
# create user
docker exec -i symfony php bin/console fos:user:create test test@example.com 123456
# grant role
docker exec -i symfony php bin/console fos:user:promote test ROLE_READER
# install assets (symlink) for dev env
docker exec -i symfony php bin/console assets:install
# clear cache
docker exec -i symfony php bin/console cache:clear --no-warmup
