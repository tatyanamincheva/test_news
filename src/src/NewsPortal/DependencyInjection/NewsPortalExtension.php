<?php

namespace NewsPortal\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;

class NewsPortalExtension implements ExtensionInterface
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        if (isset($config['results_per_page'])) {
            $container->setParameter('news_portal.results_per_page', $config['results_per_page']);

            $loader = new YamlFileLoader(
                    $container, new FileLocator(__DIR__ . '/../Resources/config')
            );
            $loader->load('services.yml');
        }
    }

    public function getAlias()
    {
        return 'news_portal';
    }

    public function getNamespace()
    {
        return 'http://www.example.com/schema/news_portal';
    }

    public function getXsdValidationBasePath()
    {
        return false;
    }

}
