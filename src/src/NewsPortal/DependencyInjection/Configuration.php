<?php

namespace NewsPortal\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('news_portal');

        $rootNode
                ->children()
                ->integerNode('results_per_page')->min(1)
                ->end()
        ;

        return $treeBuilder;
    }

}
