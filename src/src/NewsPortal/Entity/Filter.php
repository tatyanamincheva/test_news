<?php

namespace NewsPortal\Entity;

class Filter
{

    private $dateFrom;
    private $dateTo;

    public function getFilterArray()
    {
        $filter = array();
        if ($this->dateFrom) {
            $filter[] = array(
                'field' => 'date',
                'operator' => '>',
                'value' => $this->dateFrom->setTime(0, 0)
            );
        }
        if ($this->dateTo) {
            $filter[] = array(
                'field' => 'date',
                'operator' => '<',
                'value' => $this->dateTo->setTime(23, 59)
            );
        }

        return $filter;
    }

    function getDateFrom()
    {
        return $this->dateFrom;
    }

    function getDateTo()
    {
        return $this->dateTo;
    }

    function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

}
