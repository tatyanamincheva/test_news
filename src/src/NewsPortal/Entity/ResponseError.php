<?php

namespace NewsPortal\Entity;

class ResponseError
{

    const ARTICLE_NOT_FOUND = 'articleNotFound';
    const PDF_GENERATION_FAILED = 'pdfGenerationFailed';

    // later can be loaded from translations
    private $mapping = array(
        self::ARTICLE_NOT_FOUND => array(
            'message' => 'Статията не беше намерена.',
            'code' => '404'
        ),
        self::PDF_GENERATION_FAILED => array(
            'message' => 'Грешка при генерирането на PDF.',
            'code' => '500'
        ),
    );
    public $message;
    public $code;

    public function __construct($key)
    {
        $this->message = $this->mapping[$key]['message'];
        $this->code = $this->mapping[$key]['code'];
    }

}
