<?php

namespace NewsPortal\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class ThemeController extends Controller
{

    public function setAction(Request $request, $theme)
    {
        $this->get('liip_theme.active_theme')->setName($theme);
        $referer = $request->headers->get('referer');
        
        $response = new Response();
        $response->headers->setCookie(new Cookie('news_theme_ch', $theme));
        $response->sendHeaders();

        return $this->redirect($referer);
    }

}
