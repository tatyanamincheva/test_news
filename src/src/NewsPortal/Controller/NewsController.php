<?php

namespace NewsPortal\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;
use NewsPortal\Entity\Filter;
use NewsPortal\Form\FilterType;
use NewsPortal\Entity\ResponseError;

class NewsController extends Controller
{

    public function latestAction($page)
    {
        $pagination = $this->get('news_portal.news_provider')->getTodaysNewsPaged($page);

        return $this->render('NewsPortal:News:latest.html.twig', array(
                    'pagination' => $pagination
        ));
    }

    public function archiveAction(Request $request, $page)
    {
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filter = $form->getData();
        }

        $pagination = $this->get('news_portal.news_provider')->getArchivedNewsPaged($filter, $page);

        return $this->render('NewsPortal:News:archive.html.twig', array(
                    'pagination' => $pagination,
                    'form' => $form->createView()
        ));
    }

    public function viewAction($id)
    {
        $article = $this->get('news_portal.news_provider')->getArticle($id);
        if (empty($article)) {
            return $this->returnError(new ResponseError(ResponseError::ARTICLE_NOT_FOUND));
        }

        return $this->render('NewsPortal:News:view.html.twig', array(
                    'article' => $article
        ));
    }

    public function downloadPdfAction($id)
    {
        $article = $this->get('news_portal.news_provider')->getArticle($id);
        if (empty($article)) {
            return $this->returnError(new ResponseError(ResponseError::ARTICLE_NOT_FOUND));
        }

        $html = $this->renderView('NewsPortal:Common:pdf_tpl.html.twig', array(
            'article' => $article
        ));

        try {
            $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array('encoding' => 'utf-8'));
        } catch (Exception $e) {
            return $this->returnError(new ResponseError(ResponseError::PDF_GENERATION_FAILED));
        }

        return new Response($pdf, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="article_' . $id . '.pdf"'
        ));
    }

    private function returnError($error)
    {
        return $this->render('NewsPortal:Common:error.html.twig', array(
                    'error' => $error
        ));
    }

}
