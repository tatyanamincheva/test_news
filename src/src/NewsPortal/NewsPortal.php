<?php

namespace NewsPortal;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use NewsPortal\DependencyInjection\NewsPortalExtension;

class NewsPortal extends Bundle
{

    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new NewsPortalExtension();
        }

        return $this->extension;
    }

}
