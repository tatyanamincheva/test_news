<?php

namespace NewsPortal\Service;

use NewsPortal\Entity\Filter;
use DateTime;

class NewsProvider
{

    private $articleRepo;
    private $paginator;
    private $resPrePage;

    public function __construct($articleRepo, $paginator, $resPrePage)
    {
        $this->articleRepo = $articleRepo;
        $this->paginator = $paginator;
        $this->resPrePage = $resPrePage;
    }

    public function getTodaysNewsPaged($page = 1)
    {
        $filter = new Filter();
        $filter->setDateFrom(new DateTime());

        return $this->articleRepo->getFilteredArticles($filter->getFilterArray(), $this->paginator, $page, $this->resPrePage);
    }

    public function getArchivedNewsPaged($filter, $page = 1)
    {
        return $this->articleRepo->getFilteredArticles($filter->getFilterArray(), $this->paginator, $page, $this->resPrePage);
    }

    public function getArticle($id)
    {
        return $this->articleRepo->find($id);
    }

}
