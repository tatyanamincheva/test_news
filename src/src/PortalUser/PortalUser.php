<?php

namespace PortalUser;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PortalUser extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }

}
