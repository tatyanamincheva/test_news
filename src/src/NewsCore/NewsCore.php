<?php

namespace NewsCore;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use NewsCore\DependencyInjection\NewsCoreExtension;

class NewsCore extends Bundle
{

    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new NewsCoreExtension();
        }

        return $this->extension;
    }

}
