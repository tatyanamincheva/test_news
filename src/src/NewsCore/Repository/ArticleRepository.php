<?php

namespace NewsCore\Repository;

use Doctrine\ORM\EntityRepository;
use NewsCore\Entity\Article;

class ArticleRepository extends EntityRepository
{

    public function getNew()
    {
        return new Article();
    }

    public function persist($article)
    {
        $em = $this->getEntityManager();
        $em->persist($article);
        $em->flush();
    }

    public function urlExists($url)
    {
        return !empty($this->findBy(array('sourceUrl' => $url)));
    }

    public function getFilteredArticles($filters, $paginator, $page, $resPrePage)
    {
        $em = $this->getEntityManager();

        $queryStr = 'SELECT a FROM NewsCore:Article a WHERE a.id > 0';
        $params = array();

        if (!empty($filters)) {
            $i = 0; // counter added for equal field names
            foreach ($filters as $filter) {
                $i++;
                $queryStr .= ' AND a.' . $filter['field'] . ' ' . $filter['operator'] . ' :' . $filter['field'] . $i;
                $params[$filter['field'] . $i] = $filter['value'];
            }
        }

        // later may come as param
        $orderBy = ' ORDER BY a.date DESC, a.id ASC';
        $query = $em->createQuery($queryStr . $orderBy)->setParameters($params);

        return $paginator->paginate($query, $page, $resPrePage);
    }

}
