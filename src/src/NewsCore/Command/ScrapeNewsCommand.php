<?php

namespace NewsCore\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Psr\Log\LogLevel;

class ScrapeNewsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('scraper:run')
                ->setDescription('Run news scraper with parameter. Example: btv')
                ->addArgument('scrape', InputArgument::REQUIRED, 'Valid scrape optoins: btv, novinite, vesti');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $services = array(
            'btv' => 'news_core.btv_scraper',
            'novinite' => 'news_core.novinite_scraper',
            'vesti' => 'news_core.vesti_scraper'
        );

        $scrape = $input->getArgument('scrape');
        if (!empty($services[$scrape])) {
            $scraper = $this->getContainer()->get($services[$scrape]);

            $verbosityLevelMap = array(
                LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
                LogLevel::INFO => OutputInterface::VERBOSITY_NORMAL,
            );
            $logger = new ConsoleLogger($output, $verbosityLevelMap);
            $scraper->setLogger($logger);
            $output->writeln("Scraping $scrape news...");
            $scraper->scrape();
            $output->writeln('Done');
        } else {
            $output->writeln("No service configured for $scrape!");
        }
    }

}
