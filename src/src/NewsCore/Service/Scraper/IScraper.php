<?php

namespace NewsCore\Service\Scraper;

interface IScraper
{

    public function scrape();
    public function setLogger($logger);
}
