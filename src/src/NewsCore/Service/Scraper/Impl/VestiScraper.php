<?php

namespace NewsCore\Service\Scraper\Impl;

use NewsCore\Service\Scraper\IScraper;
use DateTime;
use Exception;

class VestiScraper implements IScraper
{

    const BASE_URL = 'http://www.vesti.bg';
    const MAIN_SCRAPE_PATH = '/posledni-novini';

    private $client;
    private $articleRepo;
    private $logger;
    private $artUrls;

    public function __construct($clientProvider, $articleRepo, $logger)
    {
        $this->client = $clientProvider->getClientWithCookieJar();
        $this->articleRepo = $articleRepo;
        $this->logger = $logger;
        $this->artUrls = array();
    }

    public function scrape()
    {
        $this->_collectUrls();
        $this->_filterUrls();
        $this->logger->info('Found ' . count($this->artUrls) . ' new URLs.');
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                $this->_scrapeArticle($url);
                sleep(1);
            }
        }
    }

    private function _collectUrls()
    {
        $this->logger->info('Adding URLs...');
        try {
            $crawler = $this->client->request('GET', self::BASE_URL . self::MAIN_SCRAPE_PATH);
            $articles = $crawler->filter('.content-left a.articles-list');
            if ($articles->count()) {
                $articles->each(function ($node) {
                    $this->artUrls[] = $node->attr('href');
                });
            }
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _filterUrls()
    {
        $urlsToScrape = array();
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                if (!$this->articleRepo->urlExists($url)) {
                    $urlsToScrape[] = $url;
                }
            }
        }

        $this->artUrls = $urlsToScrape;
    }

    private function _scrapeArticle($url)
    {
        $this->logger->info("Scraping URL: $url");
        try {
            $crawler = $this->client->request('GET', $url);

            $article = $this->articleRepo->getNew();
            $article->setSourceUrl($url);
            $article->setContent($this->_getContent($crawler));
            $article->setHeading($this->_getHeading($crawler));
            $article->setCategory($this->_getCategory($crawler));
            $article->setImages($this->_getImages($crawler));
            $article->setDate($this->_getDate($crawler));

            $this->articleRepo->persist($article);
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _getContent($crawler)
    {
        $content = $crawler->filter('.article-column .textfix p');
        if ($content->count() === 0) {
            return '';
        }
        $paragraphs = $content->each(function ($node) {
            return empty($node) ? '' : '<p>' . $node->html() . '</p>';
        });

        return implode('', $paragraphs);
    }

    private function _getHeading($crawler)
    {
        $heading = $crawler->filter('h1');
        if ($heading->count()) {
            return $heading->eq(0)->text();
        }

        return '';
    }

    private function _getCategory($crawler)
    {
        $category = $crawler->filter('.nav-main-ul-1 li.selected a');
        if ($category->count()) {
            return trim(mb_strtolower($category->eq(0)->text(), 'UTF-8'));
        }

        return '';
    }

    private function _getImages($crawler)
    {
        $imgNodesBig = $crawler->filter('figure.article-main-image img');
        $allImages = array();
        if ($imgNodesBig->count()) {
            $images = $imgNodesBig->each(function ($node) {
                return $images[] = $node->attr('src');
            });
            $allImages = array_merge($allImages, $images);
        }
        $imgNodes = $crawler->filter('figure.article-inner-wide-image img');
        if ($imgNodes->count()) {
            $images = $imgNodes->each(function ($node) {
                return $images[] = $node->attr('src');
            });
            $allImages = array_merge($allImages, $images);
        }

        return $allImages;
    }

    private function _getDate($crawler)
    {
        // 2016-10-01T11:15:00+03:00
        $dateNode = $crawler->filter('.article-info-left time');
        if ($dateNode->count()) {
            $rawDate = $dateNode->attr('datetime');
            $date = DateTime::createFromFormat('Y-m-d\TH:i:sP', $rawDate);

            return $date;
        }

        return new DateTime();
    }

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

}
