<?php

namespace NewsCore\Service\Scraper\Impl;

use NewsCore\Service\Scraper\IScraper;
use DateTime;
use IntlDateFormatter;
use Exception;

class NoviniteScraper implements IScraper
{

    const BASE_URL = 'http://novinite.bg';
    const MAIN_SCRAPE_PATH = '/latest/?&s=';

    private $client;
    private $articleRepo;
    private $logger;
    private $pageCounter;
    private $artUrls;

    public function __construct($clientProvider, $articleRepo, $logger)
    {
        $this->client = $clientProvider->getClientWithCookieJar();
        $this->articleRepo = $articleRepo;
        $this->logger = $logger;
        $this->pageCounter = 0;
        $this->artUrls = array();
    }

    public function scrape()
    {
        $this->_collectUrls();
        $this->_filterUrls();
        $this->logger->info('Found ' . count($this->artUrls) . ' new URLs.');
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                $this->_scrapeArticle($url);
                sleep(1);
            }
        }
    }

    private function _collectUrls()
    {
        $this->logger->info('Adding URLs...');
        try {
            $end = false;
            $fmt = new IntlDateFormatter('bg_BG', NULL, NULL);
            $fmt->setPattern('d MMMM yyyy');
            $todayStr = $fmt->format(new DateTime());
            while (!$end) {
                $crawler = $this->client->request('GET', self::BASE_URL . $this->_getNextPage());
                $articles = $crawler->filter('.news_list .item');
                if ($articles->count()) {
                    $articles->each(function ($node) use (&$end, $todayStr) {
                        $date = $node->filter('.newsdate span')->html();
                        if (strpos($date, $todayStr) !== false) {
                            $this->artUrls[] = self::BASE_URL . '/' . $node->filter('h2 a')->attr('href');
                        } else {
                            $end = true;
                        }
                    });
                } else {
                    $end = true;
                }
            }
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _filterUrls()
    {
        $urlsToScrape = array();
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                if (!$this->articleRepo->urlExists($url)) {
                    $urlsToScrape[] = $url;
                }
            }
        }

        $this->artUrls = $urlsToScrape;
    }

    private function _scrapeArticle($url)
    {
        $this->logger->info("Scraping URL: $url");
        try {
            $crawler = $this->client->request('GET', $url);

            $article = $this->articleRepo->getNew();
            $article->setSourceUrl($url);
            $article->setContent($this->_getContent($crawler));
            $article->setHeading($this->_getHeading($crawler));
            $article->setCategory($this->_getCategory($crawler));
            $article->setImages($this->_getImages($crawler));
            $article->setDate($this->_getDate($crawler));

            $this->articleRepo->persist($article);
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _getContent($crawler)
    {
        $content = $crawler->filter('#textsize p');
        if ($content->count() === 0) {
            return '';
        }
        $paragraphs = $content->each(function ($node) {
            return empty($node) ? '' : '<p>' . $node->html() . '</p>';
        });

        return implode(' ', $paragraphs);
    }

    private function _getHeading($crawler)
    {
        $heading = $crawler->filter('h1');
        if ($heading->count()) {
            return $heading->eq(0)->text();
        }

        return '';
    }

    private function _getCategory($crawler)
    {
        $category = $crawler->filter('#nav li.active a');
        if ($category->count()) {
            return trim(mb_strtolower($category->eq(0)->text(), 'UTF-8'));
        }

        return '';
    }

    private function _getImages($crawler)
    {
        $imgNodesBig = $crawler->filter('#imagebig img');
        $allImages = array();
        if ($imgNodesBig->count()) {
            $images = $imgNodesBig->each(function ($node) {
                return $images[] = $node->attr('src');
            });
            $allImages = array_merge($allImages, $images);
        }
        $imgNodes = $crawler->filter('#image img');
        if ($imgNodes->count()) {
            $images = $imgNodes->each(function ($node) {
                return $images[] = $node->attr('src');
            });
            $allImages = array_merge($allImages, $images);
        }

        return $allImages;
    }

    private function _getDate($crawler)
    {
        // 30 септември 2016, петък / 09:27
        $dateNode = $crawler->filter('.newsdate span');
        if ($dateNode->count()) {
            $rawDate = trim($dateNode->eq(0)->text());

            $fmt = new IntlDateFormatter('bg_BG', NULL, NULL);
            $fmt->setPattern('d MMMM yyyy hh:mm');
            $pattern = '/(?<date>\d+?\s\w+\s\d+)([,]\s\w+\s\/)(?<time>\s\d+?\:\d+)/u';
            preg_match($pattern, $rawDate, $matches);
            $dtStr = $matches['date'] . $matches['time'];
            $timestamp = $fmt->parse($dtStr);
            $date = new DateTime();
            $date->setTimestamp($timestamp);

            return $date;
        }

        return new DateTime();
    }

    private function _getNextPage()
    {
        $pc = $this->pageCounter;
        $this->pageCounter = $this->pageCounter + 20;
        return self::MAIN_SCRAPE_PATH . $pc;
    }

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

}
