<?php

namespace NewsCore\Service\Scraper\Impl;

use NewsCore\Service\Scraper\IScraper;
use DateTime;
use Exception;

class BtvScraper implements IScraper
{

    const BASE_URL = 'http://btvnovinite.bg';
    const MAIN_SCRAPE_PATH = '/novinite-ot-dnes?page=';

    private $client;
    private $articleRepo;
    private $logger;
    private $pageCounter;
    private $artUrls;

    public function __construct($clientProvider, $articleRepo, $logger)
    {
        $this->client = $clientProvider->getClientWithCookieJar();
        $this->articleRepo = $articleRepo;
        $this->logger = $logger;
        $this->pageCounter = 0;
        $this->artUrls = array();
    }

    public function scrape()
    {
        $this->_collectUrls();
        $this->_filterUrls();
        $this->logger->info('Found ' . count($this->artUrls) . ' new URLs.');
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                $this->_scrapeArticle($url);
                sleep(1);
            }
        }
    }

    private function _collectUrls()
    {
        $this->logger->info('Adding URLs...');
        try {
            $found = true;
            while ($found) {
                $crawler = $this->client->request('GET', self::BASE_URL . $this->_getNextPage());
                $articles = $crawler->filter('li.article .label a');
                if ($articles->count()) {
                    $articles->each(function ($node) {
                        $this->artUrls[] = self::BASE_URL . $node->attr('href');
                    });
                } else {
                    $found = false;
                }
            }
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _filterUrls()
    {
        $urlsToScrape = array();
        if (!empty($this->artUrls)) {
            foreach ($this->artUrls as $url) {
                if (!$this->articleRepo->urlExists($url)) {
                    $urlsToScrape[] = $url;
                }
            }
        }

        $this->artUrls = $urlsToScrape;
    }

    private function _scrapeArticle($url)
    {
        $this->logger->info("Scraping URL: $url");
        try {
            $crawler = $this->client->request('GET', $url);

            $article = $this->articleRepo->getNew();
            $article->setSourceUrl($url);
            $article->setContent($this->_getContent($crawler));
            $article->setHeading($this->_getHeading($crawler));
            $article->setCategory($this->_getCategory($crawler));
            $article->setImages($this->_getImages($crawler));
            $article->setDate($this->_getDate($crawler));

            $this->articleRepo->persist($article);
        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());
        }
    }

    private function _getContent($crawler)
    {
        $content = $crawler->filter('#group_8 .article_body p');
        if ($content->count() === 0) {
            return '';
        }
        $paragraphs = $content->each(function ($node) {
            return empty($node) ? '' : '<p>' . $node->html() . '</p>';
        });

        return implode(' ', $paragraphs);
    }

    private function _getHeading($crawler)
    {
        $heading = $crawler->filter('#group_4 .article_title');
        if ($heading->count()) {
            return $heading->eq(0)->text();
        }

        return '';
    }

    private function _getCategory($crawler)
    {
        $category = $crawler->filter('ul.main-menu li.selected a');
        if ($category->count()) {
            return trim(mb_strtolower($category->eq(0)->text(), 'UTF-8'));
        }

        return '';
    }

    private function _getImages($crawler)
    {
        $imgNodes = $crawler->filter('#group_8 .article_image img');
        $allImages = array();
        if ($imgNodes->count()) {
            $allImages = $imgNodes->each(function ($node) {
                return $images[] = $node->attr('src');
            });
        }

        return $allImages;
    }

    private function _getDate($crawler)
    {
        // 22.09.2016 17:44
        $dateNode = $crawler->filter('.main-wrapper .article_date');
        if ($dateNode->count()) {
            $rawDate = trim($dateNode->eq(0)->text());
            $date = DateTime::createFromFormat('d.m.Y H:i', $rawDate);

            return $date;
        }

        return new DateTime();
    }

    private function _getNextPage()
    {
        $this->pageCounter++;
        return self::MAIN_SCRAPE_PATH . $this->pageCounter;
    }

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

}
