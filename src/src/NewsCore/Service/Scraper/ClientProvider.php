<?php

namespace NewsCore\Service\Scraper;

use Goutte\Client;
use Symfony\Component\BrowserKit\CookieJar;

class ClientProvider
{

    public function getClientWithCookieJar()
    {
        return new Client(array(), null, new CookieJar());
    }

}
